#!/bin/bash

TAG=v4.2.8

docker buildx build \
--push --platform linux/arm64 \
-t registry.gitlab.com/nemushee/nemushee:${TAG} \
-t registry.gitlab.com/nemushee/nemushee \
--provenance false \
.
